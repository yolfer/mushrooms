---
title: Mushrooms are the worst food
---

Hello and welcome to my page about mushrooms. Wikipedia's wiki defines
mushrooms as follow:

> A mushroom, or toadstool, is the fleshy, spore-bearing fruiting body of a
fungus, typically produced above ground on soil or on its food source.

Mmmm, doesn't that sound tasty. A "fleshy spore-bearing fruiting body". No.
Of course it doesn't. Mushrooms are terrible. Everything about them is bad.
They taste bad, they look bad, they smell bad, they have the worst texture of
any "food", and they ruin anything they are put in.

## Looks

If you're being honest, you'll agree that most mushrooms look like shit.
Like literal poop. Not surprising, since mushrooms like to grow on poop.
For eons, animals have relied on sight, smell, and taste to stay away from
toxic, inedible food sources. Somewhere along the way someone's neurons
misfired and prompted them to eat a mushroom. They promptly died, because
it was poisonous. Fast forward a few million years, and after countless deaths
humanity unfortunately turned up some mushrooms that weren't fatally poisonous.
Wonderful. They are hideous to look at. Try cutting one open and you'll find
it even more revolting inside.

## Texture and taste

> "Mushrooms are a sponge! They soak up whatever flavor you cook it with!"

So does a dish sponge, and I don't eat those. Mushrooms have a variety of
unique textures, all of which are bad.

### The dry

Chopped up in a salad. It's like taking a bite into an eraser. The little
accordian bits are a wonderful place for spores to hang out, or for bugs
to crawl into and die once they've realized their horrible mistake.

### The chewy

Soaked in delicious ramen broth? Too bad, still too chewy to ever chew. Just
give up and swallow it and hope you don't have to eat another one.

## The slimy

When cooked, most mushrooms emit a gelatinous slime. It's disgusting, and it
ruins anything it touches. "Just pick them out!" That's like dousing your food
in hot sauce and being asked to pick out the hot sauce. It's too late, it's
already everywhere. And everything is ruined.

They are the bane of all pizza. Inevitably when pizza is being ordered for a
group, some asshole who likes mushrooms is going to order multiple pizzas with
shrooms. At the end of the party, are there any pepperoni slices left? No, of
course not, people ate those because they are good. But without fail, the
garbage slices with mushrooms will have a bunch of leftovers, because no one
likes that trash. What if you ordered things people actually liked, huh? The
only good pizza party is the one where I can order or make the pizzas. No
shrooms allowed.


# Guest contributions

I'm not the only one. Don't even try to pretend I'm the only one. I'm not
crazy. You're crazy, for thinking it's okay to inflict this disgusting fungus
on the rest of us.

Here is a contemplation of the various qualities of mushrooms:

```
Squishy, yucky, smelly, icky
We hate mushrooms cuz we so picky.

Ruin pizza, ruin noodles
You say mushrooms, I say toodles.

I eat all day, that makes me credible
If it grows on corpses, it's not edible.

No I'm not going to pick them out
Don't feed me mushrooms, dirty lout.

- Dan
```

Here is an unfortunately oft-repeated conversation one might have with a fanatical fungi apologist:

```
  A Q&A on the terribleness of mushrooms:
    Them: "Do you like mushrooms?"
    Me: "No, they're terrible."
    "Oh, you should try _____ type - they're my favorite."
    "I've tried those and many others because I want to like them but I don't. They're all terrible."
    "Oh, you should try them sauteed with butter and garlic - that's my favorite."
    "Anything that has to be covered in butter to be palatable is terrible."
    "Well, what about in a nice pasta sauce or on pizza?"
    "They still taste like mushrooms which, by the way, are terrible."
    "But you were a vegetarian - how did you like mushrooms then?"
    "I didn't. Mushrooms are terrible."
    "How abo-"
    "Let me stop you there. You won't win. Mushrooms are terrible. Fuck off."

- Jennifer
```


# Closing thoughts

First, I'd like to thank my generous and eloquent guest contributors: Thank you.

Secondly, I'd like to thank you, dear reader, for your time.

Thirdly, please feel free to cite this as a primary source when doing research
into the evils of mushrooms.

Lastly, all mushrooms can go die in a fire.
